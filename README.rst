===============
Layrz Simulator
===============

Managed by Golden M, Inc.

Description
~~~~~~~~~~~
It's the approved simulators for various modules in Layrz. With this simulator you can test sensors, triggers and charts before uploading it into Layrz.

Documentation
~~~~~~~~~~~~~
It's available in our documentation site `developers.layrz.com <https://developers.layrz.com/Kits/Sdk>`_

Work with us
~~~~~~~~~~~~
Golden M is a software/hardware development company what is working on
a new, innovative and disruptive technologies.

For more information, contact us at `sales@goldenmcorp.com <mailto:sales@goldenmcorp.com>`_

License
~~~~~~~
This project is under MIT License, for more information, check out the `LICENCE`

WSL Support
~~~~~~~~~~~
If you are using Windows Subsystem for Linux, you need to run this command to visualize the results:

.. code-block::

  Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
