=========
Changelog
=========

v1.0.2
~~~~~~
* Internal changes related to GitLab CI Automation

v1.0.1
~~~~~~
* Added support to Scatter Charts and combined Line, Area, Column and Scatter charts
